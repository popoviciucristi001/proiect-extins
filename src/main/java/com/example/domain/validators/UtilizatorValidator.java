
package com.example.domain.validators;

import com.example.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {
    public UtilizatorValidator() {
    }

    public void validate(Utilizator entity) throws ValidationException {
        String message = "";
        if (entity.getId() == null) {
            message = message + "User doesn't exist!";
            throw new ValidationException(message);
        } else {
            if (entity.getFirstName().length() == 0) {
                message = message + "First name can't be an empty string!";
            }

            if (entity.getLastName().length() == 0) {
                message = message + "Last name can't be an empty string!";
            }

            if (message.length() > 0) {
                throw new ValidationException(message);
            }
        }
    }
}
