package com.example.controller;

import com.example.utils.DBUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class SignUpController implements Initializable {

    @FXML
    private TextField tf_first_name;

    @FXML
    private TextField tf_last_name;

    @FXML
    private TextField tf_username;

    @FXML
    private TextField tf_password;

    @FXML
    private Button button_signup;

    @FXML
    private Button button_log_in;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button_signup.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!tf_username.getText().trim().isEmpty() && !tf_password.getText().trim().isEmpty() &&
                        !tf_first_name.getText().trim().isEmpty() && !tf_last_name.getText().trim().isEmpty()) {
                    DBUtils.signUpUser(event, tf_first_name.getText(), tf_last_name.getText(), tf_username.getText(), tf_password.getText());
                } else {
                    System.out.println("Fill in all information!");
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Fill in all information to sign up!");
                    alert.show();
                }
            }
        });

        button_log_in.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DBUtils.changeScene(event,"/com/example/proiect/sample.fxml", "Log In", null);
            }
        });
    }
}
