package com.example.controller;

import com.example.domain.Utilizator;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class FriendsController implements Initializable {

    @FXML
    private Button button_delete;

    @FXML
    private Button button_back;

    @FXML
    private Label label_friends;

    @FXML
    private TextField tf_firstName;

    @FXML
    private TextField tf_lastName;

    @FXML
    private TextField tf_userName;

    @FXML
    private TableView<?> tv_users;

    @FXML
    private TableColumn<?, ?> col_first_name;

    @FXML
    private TableColumn<?, ?> col_last_name;

    @FXML
    private TableColumn<?, ?> col_username;

    @FXML
    private TableColumn<?, ?> col_date;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
