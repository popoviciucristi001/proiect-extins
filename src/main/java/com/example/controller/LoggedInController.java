package com.example.controller;

import com.example.domain.FriendRequest;
import com.example.domain.Prietenie;
import com.example.domain.Utilizator;
import com.example.domain.validators.CerereValidator;
import com.example.domain.validators.PrietenieValidator;
import com.example.domain.validators.UtilizatorValidator;
import com.example.repository.db.CereriDbRepository;
import com.example.repository.db.PrietenieDbRepository;
import com.example.repository.db.UtilizatorDbRepository;
import com.example.repository.memory.InMemoryRepository;
import com.example.utils.DBUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class LoggedInController implements Initializable {
    @FXML
    private Button button_log_out;

    @FXML
    private Label label_welcome;

    @FXML
    private TextField tf_firstName;

    @FXML
    private TextField tf_lastName;

    @FXML
    private TextField tf_userName;

    @FXML
    private Button button_add;

    @FXML
    private Button button_delete;

    @FXML
    private TableView<Utilizator> tv_users;

    @FXML
    private TableColumn<Utilizator, String> col_first_name;

    @FXML
    private TableColumn<Utilizator, String> col_last_name;

    @FXML
    private TableColumn<Utilizator, String> col_username;

    @FXML
    private Button button_show_all_users;

    @FXML
    private Button button_show_friends;

    @FXML
    private Button button_show_fr;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        button_log_out.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DBUtils.changeScene(event, "/com/example/proiect/sample.fxml", "Log In", null);
            }
        });
        button_show_all_users.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showUsers();
            }
        });
        button_show_friends.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DBUtils.changeScene(event, "/com/example/proiect/friends.fxml", "Your friends", getUsername());
            }
        });
        button_show_fr.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DBUtils.changeScene(event,"/com/example/proiect/friend-requests.fxml","Friend Requests",getUsername());
            }
        });
    }

    public void setUserInformation(String username) {
        label_welcome.setText("Welcome " + username + "!");
    }

    public String getUsername() {
        String label = label_welcome.getText();
        String[] listOfStrings = label.split(" ");
        String username = listOfStrings[1];
        return username.substring(0, username.length() - 1);
    }

    public ObservableList<Utilizator> getUsersList() {
        ObservableList<Utilizator> usersList = FXCollections.observableArrayList();
        InMemoryRepository<Long, Utilizator> usersRepo = new UtilizatorDbRepository(new UtilizatorValidator(), "jdbc:postgresql://localhost:5432/academic", "postgres", "postgres");
        for (Utilizator utilizator : usersRepo.findAll()) {
            usersList.add(utilizator);
        }
        return usersList;
    }

    public ObservableList<FriendRequest> getFriendRequests() {
        ObservableList<FriendRequest> frList = FXCollections.observableArrayList();
        InMemoryRepository<Long, FriendRequest> frRepo = new CereriDbRepository(new CerereValidator(), "jdbc:postgresql://localhost:5432/academic", "postgres", "postgres");
        for (FriendRequest friendRequest : frRepo.findAll()) {
            frList.add(friendRequest);
        }
        return frList;
    }

    public void showFriendRequests(){
        ObservableList<FriendRequest> friendRequests;
        friendRequests = getFriendRequests();
        col_first_name.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        col_last_name.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        col_username.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("username"));
    }

    public ObservableList<Utilizator> getFriendsList() {
        ObservableList<Utilizator> usersList = FXCollections.observableArrayList();
        InMemoryRepository<Long, Utilizator> usersRepo = new UtilizatorDbRepository(new UtilizatorValidator(), "jdbc:postgresql://localhost:5432/academic", "postgres", "postgres");
        InMemoryRepository<Long, Prietenie> friendshipsRepo = new PrietenieDbRepository(new PrietenieValidator(), "jdbc:postgresql://localhost:5432/academic", "postgres", "postgres");

        Iterable<Utilizator> users = usersRepo.findAll();
        Iterable<Prietenie> friendships = friendshipsRepo.findAll();

        Utilizator currentUser = new Utilizator();
        for (Utilizator user : users) {
            if (Objects.equals(user.getUsername(), getUsername())) {
                currentUser = user;
            }
        }
        for (Prietenie prietenie : friendships) {
            if (Objects.equals(prietenie.getId1(), currentUser.getId())) {
                for (Utilizator user : users) {
                    if (Objects.equals(user.getId(), prietenie.getId2())) {
                        usersList.add(user);
                    }
                }
            }
            if (Objects.equals(prietenie.getId2(), currentUser.getId())) {
                for (Utilizator user : users) {
                    if (Objects.equals(user.getId(), prietenie.getId1())) {
                        usersList.add(user);
                    }
                }
            }
        }
        return usersList;
    }

    public void showUsers() {
        ObservableList<Utilizator> users;
        users = getUsersList();
        col_first_name.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        col_last_name.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        col_username.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("username"));
        tv_users.setItems(users);
    }

    public void showFriends() {
        ObservableList<Utilizator> friends;
        friends = getFriendsList();
        col_first_name.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        col_last_name.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        col_username.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("username"));
        tv_users.setItems(friends);
    }



}
