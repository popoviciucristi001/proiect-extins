package com.example.repository.db;

import com.example.domain.Prietenie;
import com.example.domain.validators.Validator;
import com.example.repository.memory.InMemoryRepository;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

public class PrietenieDbRepository extends InMemoryRepository<Long, Prietenie> {
    private final String url;
    private final String username;
    private final String password;

    public PrietenieDbRepository(Validator<Prietenie> validator, String url, String username, String password) {
        super(validator);
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public Prietenie save(Prietenie prietenie) {

        String verifySql = "select count(*) from friendships f where f.id1 = ? and f.id2 = ? and f.data = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(verifySql)) {
            ps.setLong(1, prietenie.getId2());
            ps.setLong(2, prietenie.getId1());
            String dataString = prietenie.getData().toString();
            ps.setString(3, dataString);
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();
            int ok = resultSet.getInt(1);
            if (ok == 1) {
                throw new IllegalArgumentException("The friendship already exists!");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        String sql = "insert into friendships (id1, id2, data) values (?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, prietenie.getId1());
            ps.setLong(2, prietenie.getId2());
            String dataString = prietenie.getData().toString();
            ps.setString(3, dataString);

            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Prietenie delete(Long id) {
        String sql = "delete from friendships where id=?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Set<Prietenie> findAll() {
        Set<Prietenie> friendships = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");

             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String data = resultSet.getString("data");

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate dateTime = LocalDate.parse(data, formatter);

                Prietenie prietenie = new Prietenie(id1, id2, dateTime);
                prietenie.setId(id);
                friendships.add(prietenie);
            }
            return friendships;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }
}
