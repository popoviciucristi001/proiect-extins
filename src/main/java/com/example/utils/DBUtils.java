package com.example.utils;

import com.example.controller.LoggedInController;
import com.example.domain.Utilizator;
import com.example.domain.validators.UtilizatorValidator;
import com.example.repository.db.UtilizatorDbRepository;
import com.example.repository.memory.InMemoryRepository;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.sql.*;

import java.io.IOException;

public class DBUtils {
    public static void changeScene(ActionEvent event, String fxmlFile, String title, String username) {
        Parent root = null;
        if (username != null) {
            try {
                FXMLLoader loader = new FXMLLoader(DBUtils.class.getResource(fxmlFile));
                root = loader.load();
                LoggedInController loggedInController = loader.getController();
                loggedInController.setUserInformation(username);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                root = FXMLLoader.load(DBUtils.class.getResource(fxmlFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle(title);
        stage.setScene(new Scene(root, 600, 400));
        stage.show();
    }

    public static void signUpUser(ActionEvent event, String firstName, String lastName,
                                  String username, String password) {
        Connection connection = null;
        PreparedStatement psInsert = null;
        PreparedStatement psCheckUsersExists = null;
        ResultSet resultSet = null;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/academic", "postgres", "postgres");
            psCheckUsersExists = connection.prepareStatement("SELECT * FROM users WHERE username = ?");
            psCheckUsersExists.setString(1, username);
            resultSet = psCheckUsersExists.executeQuery();
            if (resultSet.isBeforeFirst()) {
                System.out.println("The username is already taken!");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("The username is already taken!");
                alert.show();
            } else {
                psInsert = connection.prepareStatement("INSERT INTO users (first_name, " +
                        "last_name, username, password) VALUES (?,?,?,?)");
                psInsert.setString(1, firstName);
                psInsert.setString(2, lastName);
                psInsert.setString(3, username);
                psInsert.setString(4, password);
                psInsert.executeUpdate();
                changeScene(event, "/com/example/proiect/logged-in.fxml", "welcome", username);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (psCheckUsersExists != null) {
                try {
                    psCheckUsersExists.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (psInsert != null) {
                try {
                    psInsert.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void logInUser(ActionEvent event, String username, String password) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/academic", "postgres", "postgres");
            preparedStatement = connection.prepareStatement("SELECT password FROM users where username = ?");
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                System.out.println("User not found in the DB!");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("User not found in the DB!");
                alert.show();
            } else {
                resultSet.next();
                    String retrievedPassword = resultSet.getString("password");
                    if (retrievedPassword.equals(password)) {
                        changeScene(event, "/com/example/proiect/logged-in.fxml", "Welcome!", username);
                    } else {
                        System.out.println("Passwords didn't match!");
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setContentText("Passwords didn't match!");
                        alert.show();
                    }
                }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}


